package com.techelevator;

import java.sql.SQLOutput;
import java.util.Scanner;

/*
 Write a command line program which prompts the user for the total bill, and the amount tendered. It should then
 display the change required.

 $ java MakeChange
 Please enter the amount of the bill: 23.65
 Please enter the amount tendered: 100.00
 The change required is 76.35
 */
public class MakeChange {

	public static void main(String[] args) {

		Scanner keyboard = new Scanner(System.in);
		double costOfBill = 0;
		double amountTendered = 0;
		double changeRequired = 0;

		String input = "";

		System.out.println("Enter total amount of bill, please.");
		input = keyboard.nextLine();
		costOfBill = Double.parseDouble(input);

		System.out.println("Enter total amount tendered.");
		input = keyboard.nextLine();
		amountTendered = Double.parseDouble(input);

		changeRequired = (amountTendered - costOfBill);
		System.out.printf("Total cost of bill is %4.2f\n", costOfBill);
		System.out.printf("Total amount tendered is %4.2f\n", amountTendered);
		System.out.printf("Change required is %4.2f\n", changeRequired);




	}

}
