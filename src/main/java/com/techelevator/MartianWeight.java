package com.techelevator;
/*
 In case you've ever pondered how much you weight on Mars, here's the calculation:
 	Wm = We * 0.378
 	where 'Wm' is the weight on Mars, and 'We' is the weight on Earth
 
Write a String[] earthWeightSpaces =
and displays each Earth weight as itself, and its Martian equivalent.

 $ MartianWeight 
 
Enter a series of Earth weights (space-separated): 98 235 185
 
 98 lbs. on Earth, is 37 lbs. on Mars.
 235 lbs. on Earth, is 88 lbs. on Mars.
 185 lbs. on Earth, is 69 lbs. on Mars. 
 */
import java.util.Scanner;

public class MartianWeight {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.print("Enter a series of Earth weights (space-separated): ");

		String earthWeight = input.nextLine();

		String[] earthWeightSpaces = earthWeight.split(" ");


		for (int i = 0; i < earthWeightSpaces.length; i++) {

			double marsWeight = Double.parseDouble(earthWeightSpaces[i]);

			System.out.println(earthWeight + " lbs. on Earth, is " + (marsWeight * .378) + " lbs. on Mars.");


		}

	}

}
